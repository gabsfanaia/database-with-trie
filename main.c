#include "inserir/insere.h"
#include "trie/trie.h"
#include "busca/procura.h"

int main(int argc, char **argv) {
	struct trie_node *root = create_node();
	struct list *list = create_list();
	char *base = argv[1];
	char *arg = argv[2];

	if (argc != 3) {
		fprintf(stderr, "Número incorreto de argumentos. Uso: %s <arg1> <arg2>\n", argv[0]);
		exit(1);
	}

	if (strcmp(argv[0], "./insere") == 0) {
		program_insere(base, arg, root);

	} else if (strcmp(argv[0], "./procura") == 0) {
		program_procura(list, root, arg, base);
	}

	free_trie(root);
	free(list);
	return 0;
}


