#ifndef _PROCURA_
#define _PROCURA_

#include "../trie/trie.h"

struct namefile {
	char *name_file;
	struct namefile *next;
};

struct list {
	struct namefile *head;
};

void program_procura(struct list *list, struct trie_node *root, char *arg, char *base);
struct list *create_list();

#endif
