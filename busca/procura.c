#include "procura.h"
#include "../trie/trie.h"
#include "../inserir/insere.h"

/*creat_list - lista para guardar os nomes dos arquivos que serao imprimidos*/
struct list *create_list() {
	struct list *list = (struct list*) malloc(sizeof(*list));
	if(!list) exit(1);

	list->head = NULL;

	return list;
}

struct namefile *create_node_namefile() {
	struct namefile *new = malloc(sizeof(*new));
	if (!new) exit(1);

	new->name_file = NULL;
	new->next = NULL;

	return new;
}

/*compare_name_file - verifica se nao ha repeticao de nomes de arquivos na lista
 * que sera imprimida
 * @node - ponteiro para um nodo da trie
 * @name - nome do arquivo que sera inserido*/
int compare_namefiles(struct namefile *node, char *name) {
	if (strcmp(name, node->name_file) == 0) {
		return 1;
	}

	return 0;
}

/*store_name_file - insere o nome do arquivo na lista ligada
 * @node - ponteiro para um nodo da arvore trie
 * @name - nome do arquivo */
void store_name_file(struct list *list, char *name) {
	if (list->head == NULL) {
		list->head = create_node_namefile();
		list->head->name_file = strdup(name);
	} 

	struct namefile *aux = list->head;

	while(aux->next != NULL) {
		if (compare_namefiles(aux, name))
			return;

		aux = aux->next;
	}
	
	if (compare_namefiles(aux, name))
		return;

	aux->next = create_node_namefile();
	if (!aux->next) exit(1);

	aux->next->name_file = strdup(name);
}

void output(struct list *list, char *prefix) {
	struct namefile *aux = list->head;
	while (aux != NULL) {
		printf("%s %s \n", aux->name_file, prefix);
		aux = aux->next;

	}
}

/*find_words_with_prefix - procura na arvore, palavras que contem o prefixo
 * dado e insere o nome na lista ligada
 * @node - ponteiro para um nodo da arvore
 * @prefix - prefixo dado
 * @buffer - usado para guardar as palavras que contem os prefixo
 * @size - controla o tamanho da string que guardara as palavras como prefixo*/
void find_words_with_prefix(struct trie_node *node, char *prefix, char *buffer, int size
		, struct list *list) {
	if (node == NULL) 
		return;

	if (node->end) {
		buffer[size] = '\0';
		for (int j = 0; j < node->number_of_files; j++)
			store_name_file(list, node->name_file[j]);
	}

	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (node->letter[i]) {
			char ch;
			if (i < 26) {
				ch = 'a' + i;
			} else {
				ch = 'A' + i - 26;
			}
			buffer[size] = ch;
			find_words_with_prefix(node->letter[i], prefix, buffer, size + 1, list);
		}
	}
	
}

/* search_prefix - procura o prefixo na arvore para buscar palavras que a contem
 * @prefix - prefixo dado
 * @list - ponteiro para lista que guardara os nomes dos arquivos
 */
int search_prefix(struct trie_node *root, char *prefix, struct list *list) {
	struct trie_node *current_node = root;
	char buffer[BUFFER_MAX];
	int size = 0;

	for (int i = 0; prefix[i] != '\0'; i++) {
		int index;
		if (prefix[i] >= 'a' && prefix[i] <= 'z') {
			index = prefix[i] - 'a';
		} else if (prefix[i] >= 'A' && prefix[i] <= 'Z') {
			index = prefix[i] - 'A' + 26;
		} else {
			return 0;
		}

		if (!current_node->letter[index]) {
			return 0;
		}
		buffer[size] = prefix[i];
		size++;
		current_node = current_node->letter[index];
	}

	find_words_with_prefix(current_node, prefix, buffer, size, list);

	output(list, prefix);

	return 1;
}


void free_list(struct list *list) {
	struct namefile *aux = list->head;
	struct namefile *remove;

	while(aux != NULL) {
		remove = aux;
		aux = aux->next;
		free(remove->name_file);
		free(remove);
	}
}

/* program_procura - funcao chamda na main procura o prefixo colocado na linha de comando
 * apartir do banco de dados
 * @list - ponteiro para lista ligada que guardara os nomes dos arquivos
 * @root - ponteiro para um nodo da trie
 * @arg - prefixo colocado na linha de comando
 * @base - ponteiro para o banco de dados*/
void program_procura(struct list *list, struct trie_node *root, char *arg,
		char *base) {
	FILE *database = fopen(base , "r+");
		if (database) {
			deserialize_trie(database, root);
			rewind(database);
		} 
		
		search_prefix(root, arg, list);
		free_list(list);

		fclose(database);
}


