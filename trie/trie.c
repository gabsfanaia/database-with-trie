#include "trie.h"

struct trie_node *create_node() {
	struct trie_node *new_node = (struct trie_node*) malloc(sizeof(*new_node));
	if (!new_node) {
		fprintf(stderr, "Erro ");
		exit(1);
	}

	new_node->end = 0;
	new_node->number_of_files = 0;
	new_node->name_file = NULL;
	for (int i = 0; i < ALPHABET_SIZE; i++) {
		new_node->letter[i] = NULL;       
	}

	return new_node;
}

void free_trie(struct trie_node *node) {

	if (node) {
		for (int i = 0; i < ALPHABET_SIZE; i++) {
			free_trie(node->letter[i]);
		}

		if (node->name_file) {
			for (int j = 0; j < node->number_of_files; j++) {
				free(node->name_file[j]);
			}
			free(node->name_file);
		}

		free(node);
	}
}
