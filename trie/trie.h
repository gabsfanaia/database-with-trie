#ifndef _TRIE_
#define _TRIE_

#define ALPHABET_SIZE 52 

#include <stdio.h>
#include <stdlib.h>

struct trie_node {
	struct trie_node *letter[ALPHABET_SIZE];
	char **name_file;
	int number_of_files;
	int end;
};

struct trie_node *create_node();
void free_trie(struct trie_node *node);

#endif
