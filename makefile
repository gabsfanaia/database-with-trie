CC = gcc
CFLAGS = -Wall -Wextra 

SOURCES = main.c inserir/insere.c busca/procura.c trie/trie.c

OBJECTS = $(SOURCES:.c=.o)

all : insere procura

insere: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o insere

procura: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o procura

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)

.PHONY: clean
