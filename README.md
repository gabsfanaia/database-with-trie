# Trie

Implementação da árvore trie para inserção e busca em um banco de dados apartir de textos alvos.
O intuito da implementação é inserir partes de textos, com o programa insere, em um banco de dados estruturado apartir de uma árvore trie. A busca do prefixo será feita dentro do banco de dados apatir dos textos ja inseridos, informando se existem palavras com tal prefixo e em quais textos encontra-se.

# Utilização
- Programa insere na linha de comando:

        ./insere database texto

* database: nome do banco de dados(caso esteja fora do diretório existente, é necessário passar o caminho até o arquivo). Caso não exista, ele será criado(um arquivo de texto simples sem um tipo definido) no mesmo diretório.
* texto: o nome do texto que será inserido no banco de dados(caso esteja fora do diretório existente, é necessário passar o caminho até o arquivo).
- Programa procura na linha de comando

      ./procura database prefixo

* prefixo: prefixo que será procurado no banco de dados.
A saída é vazia se o prefixo não for encontrado. Caso seja encontrado, os nomes dos textos que ele aparece é listado linha a linha.

## Implementação

### Visão Geral
O programa le diversos textos e o insere de maneira serializada em um banco de dados, de forma que cada palavra contenha o nome do texto de aparição (pode ser mais de um texto). Na busca, o prefixo informado é buscado, e todos palavras com aquele prefixo aparecem com o nome dos textos de suas respectivas aparições.

### Programa insere

- Estrutura utilizada:
    uma struct do nodo da trie que contém, um ponteiro para os índices da árvore, um inteiro para indicar o final da palavra, um vetor de strings para os nomes dos arquivos e um inteiro para o numero de nomes dos arquivos.

- Leitura do Banco de Dados:
        a leitura do banco de dados é feita e colocada na árvore - deserialização(explicação abaixo) - para servir de base para a leitura dos próximos arquivos.

- Leitura do Texto:
        o texto é lido letra a letra e verifica se a letra pertence a-zA-z para ser colocado em um buffer, se é um delimitador (ex: ',''.'' ''\n'etc..) para verificar o final da palavra ou se é uma letra com pontuacão para ser ignorada e continuar a leitura da palavra. Dentro da leitura, o índice da palavra ja é verificado para ser inserido na árvore(>3), e o nodo da ultima letra é retornado para ser inserido o nome do texto o qual a palavra pertence. Há verificação nos nomes dos arquivos para que os nomes não sejam repetidos.

- Serialização:
    Depois da leitura de todo texto, a arvore é serializada para um arquivo.
    '/' significa o final da palavra e ')' a quantidade de recurções realizadas na árvore após a inserção.
    Após o final da palavra, todos os nomes dos arquivos em que apalavra aparece são colocados, separados por vírgula e com terminação \*asterisco\* para dizer que não existem mais nomes de arquivos.

``` bash
Ex: bola bolacha estão no texto 2
bola banana estão no texto 3
serializacao:banana/texto3\*)))))ola/texto2,texto3\*cha/texto2\*))))))))
```

- Deserialização 
    Na deserialização, o arquivo é lido letra a letra, inserido na árvore, ao final da palavra '/', todos os arquivos são colocados no vetor de strings até o '\*', ao encontrar o ')' a árvore faz chamadas recursivas até o prefixo em questão para continuar inserindo na árvore.

### Programa Procura 

- Estrutura utlizada:
    uma lista ligada que contem os nomes dos arquivos

- Leitura do Banco de Dados:
    o banco de dado é lido-deserializado- e colocado na árvore para fazer a busca.

- Procura:
    o prefixo é colocado em um buffer e em seguida é chamada uma função para procurar todas palavras na arvore que o contém. Ao final de todas as palavras, os nomes dos arquivos são colocados na lista sem repetições. Após isso, a lista é printada nome a nome com o prefixo na frente.

### Autor 

Estudante: Gabriela Fanaia
GRR: 20220070
