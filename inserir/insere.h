#ifndef _INSERE_
#define _INSERE_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../trie/trie.h"

#define FIELD_MAX 1024
#define BUFFER_MAX 4096

void program_insere(char *base, char *text, struct trie_node *root);
void deserialize_trie(FILE* file, struct trie_node* root);

#endif
