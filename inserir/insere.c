#include "../trie/trie.h"
#include "insere.h"

/* insert_name_file - insere o nome do arquivo na arvore
 * @node - ponteiro para um nodo da trie
 * @name - nome do arquivo a ser inserido*/
void insert_name_file(struct trie_node *node, char *name) {
	name[strcspn(name, "\n")] = '\0';
	if (node->number_of_files == 0) {
		node->name_file = malloc(sizeof(char*));
	} else {
		node->name_file = realloc(node->name_file, (node->number_of_files + 1) * sizeof(char*));
	}

	node->name_file[node->number_of_files] = strdup(name);
	node->number_of_files++;

}

/* insert_word - insere a palavra na trie
 *  @root - ponteiro para um nodo da trie
 *  @word - palavra a ser inserida*/
struct trie_node *insert_word(struct trie_node *root, char *word) {
	struct trie_node *current_node = root;
	if (root == NULL) {
		return NULL;
	}

	for (int i = 0; word[i] != '\0'; i++) {
		int index;
		if (word[i] >= 'a' && word[i] <= 'z') {
			index = word[i] - 'a';
		} else if (word[i] >= 'A' && word[i] <= 'Z') {
			index = word[i] - 'A' + 26; 
		} else {
			continue;
		}
		if (!current_node->letter[index]) {
			current_node->letter[index] = create_node();
		}
		current_node = current_node->letter[index];
	}
	current_node->end = 1;

	return current_node;
}

void initialize_fields(char **field) {
	for (int i = 0; i < FIELD_MAX; i++) {
		field[i] = NULL;
	}
}

/* check_name_file - verifica se o nome do arquivo não tera o nome duplicado na arvore
 *  @node - ponteiro para um nodo da arvore trie
 *  @name_file - string que guarda o nome do arquivo*/
int check_file_names(struct trie_node *node, char *name_file) {
	for (int i = 0; i < node->number_of_files; i++) {
		if (strcmp(name_file, node->name_file[i]) == 0) {
			return 1;
		}
	}

	return 0;
}

/* serialize_trie - le a arvore trie e a serializa no banco de dados
 * @file - arquivo em qual a arvore vai ser inserida serializada
 * @root - ponteiro para um nodo da arvore trie*/
void serialize_trie(FILE* file, struct trie_node *root) {
	if (root->end) {
		fputc('/', file);
		for (int i = 0; i < root->number_of_files; i++) {
			fprintf(file,"%s",root->name_file[i]);
			if (i < root->number_of_files-1) {
				fputc(',', file);
			} else 
				fputc('*', file);
		}
	}

	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (root->letter[i] && i < 26) {
			fputc(i + 'a', file);
			serialize_trie(file, root->letter[i]);
		} else if (root->letter[i] && i >= 26) {
			fputc(i + 'A' - 26, file);
			serialize_trie(file, root->letter[i]);
		}
	}
	fputc(')', file);
}

/* read_name_file - le os nomes de todos os arquivos apos o final de uma palavra no banco de dados
 * @file - ponteiro para o arquivo que esta sendo lido
 * @node - ponteiro para um nodo da trie*/
void read_name_files(FILE *file, struct trie_node *node) {
	char c;
	int i = 0;
	char name[50];

	while ((c = fgetc(file)) != EOF) {
		if (c != ',' && c != '*') {
			name[i] = c;
			i++;
		}

		if (c == '*') {
			name[i] = '\0';
			insert_name_file(node, name);
			return;
		} else if (c == ',') {
			name[i] = '\0';
			insert_name_file(node, name);
			i = 0;
		}
	}

}

/*deserialize_trie - deserealiza a arvore/banco de dados e insere na trie
 * @file - ponteiro para o banco de dados
 * @root - ponteiro para a raiz da arvore trie*/
void deserialize_trie(FILE* file, struct trie_node* root) {
	char word[50]; 
	int i = 0;
	char c;

	while ((c = fgetc(file)) != EOF) {
		if (c != '/' && c != ')') {
			word[i] = c;
			i++;
		}

		if (c == '/') {
			word[i] = '\0';
			struct trie_node *current = insert_word(root, word);
			read_name_files(file, current);
		}

		if (c == ')'){
			i--;
		}  
	}
}

/* read_text - funcao le o texto passado na linha de comando
 * @root - ponteiro para raiz da arvore trie
 * @text - pponteiro para o texto a ser lido
 * @name_file - string que contem o nome do arquivo que esta sendo lido*/
void read_text(struct trie_node *root, FILE *text, char *name_file) {
	char c;
	char word[50];
	int i = 0;

	while ((c = fgetc(text)) != EOF) {
		if (c >= 'a' && c <= 'z') {
			word[i] = c;
			i++;
		} else if (c >= 'A' && c <= 'Z') {
			word[i] = c;
			i++;
		} else if (c == ' ' || c == ',' || c == '.' || c == ':' || c == '!' ||
				c == '?'|| c == '\n') {
			word[i] = '\0';
			if (i > 3) {
				struct trie_node *current = insert_word(root, word);	
				if (!check_file_names(current, name_file))
					insert_name_file(current, name_file);
			}
			i = 0;
		} else {
			continue;
		}
	}
}

/*program_insere - funcao chamada na main para ler o texto e inserir no banco de dados
 * @base: string que garda o nome do banco de dados
 * @arg: string que guarda o nome do arquivo que vai ser lido
 * @root: ponteiro para raiz da arvore trie*/
void program_insere(char *base, char *arg, struct trie_node *root) {
	FILE *database = fopen(base, "r+");
		if (database) {
			deserialize_trie(database, root);
			rewind(database);
		} else 
			database = fopen(base, "w+");

		FILE *text = fopen(arg, "r");
		if (!text) 
			exit(2);

		read_text(root, text, arg);
		serialize_trie(database, root);
		
		fclose(database);
		fclose(text);

}
